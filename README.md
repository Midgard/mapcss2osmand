# subber: subtitling with mpv

## Introduction

A good subtitling workflow can mean the difference between 4 hours and 5 hours of work. subber's
authors didn't find a good workflow with existing subtitling software, and found that for
creating a simple sub, a simple script for mpv suffices and is actually *more* ergonomic than the
existing libre subtitling suites.

Make sure to read the *Basic usage* section below to get started! If you want to level up, come back
here to read *Advanced usage*.

## Installation

Put `subber.lua` in your mpv's `scripts` folder. On Linux and similar systems, this is typically
`~/.config/mpv/scripts`.

## Basic usage

Press `x` at the time in the media where you want to start a subtitle, press `c` on the frame after
you want it to end, then enter the text. The text is written immediately to the subtitle file,
which is then reloaded, so if you seek backwards, it will appear.

Use `shift+x` or `shift+c` instead to start or finish the subtitle 200 ms before the current
playback timestamp.

Use `F4` to play/pause the time region for the subtitle you're currently editing. If no subtitle is
being edited, this starts playing from the timestamp that's currently marked.

Use `ctrl+c` to copy the current playback timestamp to the system clipboard (using `wl-copy`, only
on systems using Wayland).

## Advanced usage

`shift+F4` is like `F4` but always starts from the beginning of the time region.

`shift+←` / `shift+→` to seek backwards / forwards by 1 second. (These are default keys in mpv.)

`F3` / `F5` to seek backwards / forwards within the time region of the subtitle you're
currently editing.

`shift+F3` / `shift+F5` to seek to the start / end of the time region of the subtitle you're
currently editing.

`F1` or `shift+F1` / `F10` or `shift+F10`, to play 1 or 2.5 seconds before / after the end of
the time region.

Use `ctrl+r` to manually reload the subtitles file.

## Supported types

If a subtitle file is selected in mpv, that one will be written to. Otherwise, subber will attempt
to create an ASS file in the same directory as the media file.

Supported formats:
- ASS (Advanced SubStation Alpha format)
- WebVTT
- A simple format that's a mix between ASS and WebVTT, reported as "sub", but I forgot what it is
  called in full, and it's not easy to find online. If you know, let us know

## Hints

To finetune timings, you can use mpv's built-in `,` and `.` to seek frame-wise. ASS subtitles
should be accurate to the frame for common frame rates.

When adding a subtitle, the amount of characters per second (CPS) will be shown as you type.

## Configuration

This script exposes some options which can be set by creating `script-opts/subber.conf` in your mpv
user folder:
```
# When to start displaying the characters per second in red
cps-warn=22

# Amount of seconds to subtract from the current timestamp when pressing shift+x / shift+c
offset=0.15
# Amount of seconds to seek with F3 / F5
seek=1
# Amount of seconds to peek with F1 / F10
peek=1
# Amount of seconds to peek with shift+F1 / shift+F10
peek-more=2.5
```

These options can also be altered on the command line with e.g.
`--script-opts=subber-cps-warn=10,subber-offset=0.1`

## Limitations

- Editing subtitles in mpv is not possible. However, for corrections during editing, you can
  open the subtitle file in a text editor and edit the text and timings by hand (use subber's
  `ctrl+c`). Ideally this text editor should support auto-reloading files when they have been
  modified by another program, to prevent overwriting new subtitle lines. After saving the file,
  use subber's `ctrl+r`. For larger modifications, a subtitle editor such as Aegisub may be
  preferable.

- `ctrl+c` currently works only with `wl-copy`, and hence only on Wayland. If you know a little bit
  of programming, the source code is easy to modify to work with another system.

- Subtitles that are embedded in the media file cannot be created.

- Small caveat: if an ASS subtitle file exists that is not readable but is writable, it will be
  overwritten. You can blame the lack of a "file exists" function in Lua's standard library for
  this.

- It's possible that for some FPS rates, the times aren't always frame perfect. Currently I haven't
  found the motivation to dive into this; you're welcome to help out!

## Contact

If you have feedback or just want to say that you find this script useful, I'd love to hear from
you! Maybe you can find me on IRC in [#mpv on Libera.Chat](https://web.libera.chat/#mpv), or you
can send a PM to `midgard`. You can also send an email to the email address I used to commit this
code!
